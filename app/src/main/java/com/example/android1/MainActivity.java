package com.example.android1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView numberToIncrease;
    Button increase;
    Button decrease;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numberToIncrease = findViewById(R.id.numberToIncrease);
        increase = findViewById(R.id.increase);
        decrease = findViewById(R.id.decrease);

        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = Integer.parseInt(numberToIncrease.getText().toString());
                numberToIncrease.setText(String.valueOf(a + 1));
            }
        });
        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = Integer.parseInt(numberToIncrease.getText().toString());
                numberToIncrease.setText(String.valueOf(a - 1));
            }
        });

    }
}
